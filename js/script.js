let listTabs = document.querySelector('.tabs');

listTabs.addEventListener('click', listAttr);


function listAttr(event) {

    if (event.target.closest('.tabs-title')) {
        let contentTabs = document.querySelector('.tabs-content');
        let title = event.target.closest('.tabs-title').dataset.title;

        Array.from(listTabs.children).forEach(button => button.classList.remove('active'));
        event.target.closest('.tabs-title').classList.add('active');

        Array.from(contentTabs.children).forEach(tab => tab.dataset.name === title ? tab.classList.add('active') : tab.classList.remove('active'))
    }
}

